package utils

import (
	"github.com/Sirupsen/logrus"
	"local.seward.gd/models"
	"net/http"
)

// GetStatusCode func get Http code from error
func GetStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}
	logrus.Error(err)
	switch err {
	case models.ErrInternalServerError:
		return http.StatusInternalServerError
	case models.ErrNotFound:
		return http.StatusNotFound
	default:
		return http.StatusInternalServerError
	}
}

