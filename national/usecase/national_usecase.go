package usecase

import (
	"context"
	"local.seward.gd/models"
	"local.seward.gd/national"
	"time"
)

type nationalUsecase struct {
	nationalRepo national.Repository
	contextTimeout time.Duration
}

//NationalUsecase will create new an nationalUsecase object representation of national.Usecase interface
func NationalUsecase(nr national.Repository, timeout time.Duration) national.Usecase {
	return &nationalUsecase{
		nationalRepo: nr,
		contextTimeout: timeout,
	}
}

func (m *nationalUsecase) Fetch(c context.Context) ([]models.NullString, error) {
	ctx, cancel := context.WithTimeout(c, m.contextTimeout)
	defer cancel()

	listNationalities, err := m.nationalRepo.Fetch(ctx)
	var result = make([]models.NullString, 0)

	for i := 0; i < len(listNationalities); i++ {
		tmp := listNationalities[i]
		result = append(result, tmp.Country)
	}
	if err != nil {
		return nil, err
	}
	return result, err
}