package national

import (
	"context"
	"local.seward.gd/models"
)

type Repository interface {
	Fetch(ctx context.Context) ([]*models.National, error)
}
