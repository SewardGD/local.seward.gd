package main

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/spf13/viper"
	"local.seward.gd/db"
	"local.seward.gd/middleware"
	"log"
	"net/http"
	"time"

	_nationalRepo "local.seward.gd/national/repository"
	_nationalUcase "local.seward.gd/national/usecase"
	_nationalHttpDeliver "local.seward.gd/national/delivery/http"
)

func init()  {
	viper.SetConfigFile("config.json")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	if viper.GetBool("debug") {
		fmt.Println("Service Run On DEBUG mode")
	}
}

func main()  {
	masterDB := db.GetMasterDB()
	slaveDB := db.GetSlaveDB()

	defer func() {
		err := masterDB.Close()
		if err != nil {
			log.Fatal(err)
		}

		err = slaveDB.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	e := echo.New()
	middle := middleware.InitMiddleware()
	e.Use(middle.CORS)
	timeoutContext := time.Duration(viper.GetInt("context.timeout")) * time.Second

	nr := _nationalRepo.NewMysqlNationalRepository(masterDB, slaveDB)
	unu := _nationalUcase.NationalUsecase(nr, timeoutContext)
	_nationalHttpDeliver.NewNationalHandler(e, unu)

	s := &http.Server{
		Addr: "127.0.0.1:9090",
		ReadTimeout: 5*time.Second,
		WriteTimeout: 5*time.Second,
	}

	e.Logger.Fatal(e.StartServer(s))
}

